const { Client } = require('pg')
const fs = require('fs');
const path = require('path');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'myDatabase',
    password: '1988',
    port: 5432,
});

function matchesPerCity() {
    client.connect();
    client.query("select city,count(city) as matchesPlayed from worldcupmatches group by city;", (err, data) => {
        if (err) console.error();
        else {
            fs.writeFileSync(
                path.join(__dirname, "./1-matchesPerCity.json"),
                JSON.stringify(data.rows),
                'utf-8'
            );
        }
        client.end();
    });
}
module.exports = matchesPerCity;
