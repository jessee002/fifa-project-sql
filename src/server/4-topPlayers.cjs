const { Client } = require('pg')
const fs = require('fs');
const path = require('path');
const { pseudoRandomBytes } = require('crypto');
const { group } = require('console');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'myDatabase',
    password: '1988',
    port: 5432,
});
const q1 = `SELECT player_name,
            ROUND((SUM(CASE WHEN Event LIKE '%G%' THEN 1 ELSE 0 END) * 100.0 / COUNT(*))::numeric, 2) AS probability
            FROM worldcupplayers
            GROUP BY player_name 
            HAVING COUNT(*) > 5
            ORDER BY probability DESC limit 10;`

function topPlayers() {
    client.connect();

    client.query(q1, (err, data) => {
        if (err) console.error(err);
        else {
            console.log(data.rows);
            fs.writeFileSync(
                path.join(__dirname, "./topPlayersResult.json"),
                JSON.stringify(data.rows),
                'utf-8'
            );
        }
        client.end();
    });
}
module.exports = topPlayers;
