const { Client } = require('pg')
const fs = require('fs');
const path = require('path');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'myDatabase',
    password: '1988',
    port: 5432,
});
const q1 = `select (
                case 
                when worldcupmatches.home_team_initials = worldcupplayers.team_initials 
                then worldcupmatches.home_team_name
                else worldcupmatches.away_team_name
                end
                ) as team_name, count(team_initials)
                FROM worldcupmatches
                Inner join worldcupplayers ON worldcupmatches.MatchID = worldcupplayers.MatchID
                WHERE (worldcupplayers.Event LIKE '%R%' and worldcupmatches.Year = 2014)
                GROUP BY team_name;`

function redCardsIssued() {
    client.connect();

    client.query(q1, (err, data) => {
        if (err) console.error(err);
        else {
            console.log(data.rows);
            fs.writeFileSync(
                path.join(__dirname, "./redCardIssuedResult.json"),
                JSON.stringify(data.rows),
                'utf-8'
            );
        }
        client.end();
    });
}
module.exports = redCardsIssued;
