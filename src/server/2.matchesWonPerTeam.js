const { Client } = require('pg')
const fs = require('fs');
const path = require('path');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'myDatabase',
    password: '1988',
    port: 5432,
});
const q1 = 
`SELECT
   home_team_name,
   COUNT(CASE
         WHEN home_team_goals > away_team_goals THEN 1
         WHEN home_team_goals = away_team_goals AND win_conditions LIKE '%(penalty%)%' AND win_conditions LIKE '%home_team_name%' THEN 1
         ELSE NULL
      END
   ) AS Matches_Won
FROM
   WorldCupMatches
GROUP BY
   home_team_name

UNION ALL

SELECT
   away_team_name,
   COUNT(CASE
         WHEN away_team_goals > home_team_goals THEN 1
         WHEN home_team_goals = away_team_goals AND win_conditions LIKE '%(penalty%)%' AND win_conditions LIKE '%away_team_name%' THEN 1
         ELSE NULL
      END
   ) AS Matches_Won
FROM
   worldcupmatches
GROUP BY
   away_team_name
ORDER BY
   Matches_Won DESC;`
            
function matchesWonPerTeam() {
    client.connect();

    client.query(q1, (err, data) => {
        if (err) console.error(err);
        else {
            console.log(data.rows);
            fs.writeFileSync(
                path.join(__dirname, "./matchesWonPerTeamJSon.json"),
                JSON.stringify(data.rows),
                'utf-8'
            );
        }
        client.end();
    });
}
module.exports = matchesWonPerTeam;
